const ACTIONS = {
  PRODUCT_LIST: '@PRODUCT/PRODUCT_LIST',
  GET_PRODUCTS: '@PRODUCT/GET_PRODUCTS',
};

const setProductList = products => ({
  type: ACTIONS.PRODUCT_LIST,
  payload: { products },
});

const requestGetProducts = (id = 0) => ({
  type: ACTIONS.GET_PRODUCTS,
  payload: { id },
})

export default {
  ...ACTIONS,
  setProductList,
  requestGetProducts,
};
