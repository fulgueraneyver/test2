import productActions from './product';
import orderActions from './order';

export {
  productActions,
  orderActions,
};
