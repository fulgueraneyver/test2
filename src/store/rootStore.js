import { addMiddleware } from 'redux-dynamic-middlewares';

import productReducer from '../views/Products/reducer';
import orderReducer from '../views/Orders/reducer';
import productMiddleware from '../views/Products/middleware';
import orderMiddleware from '../views/Orders/middleware';

import initializeStore from './initializeStore';

const rootStore = initializeStore();

rootStore.injectReducer('product', productReducer);
rootStore.injectReducer('order', orderReducer);

addMiddleware(productMiddleware);
addMiddleware(orderMiddleware);

export default rootStore;
