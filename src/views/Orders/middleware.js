import { OrdersService } from '../../services';
import { createMiddleware } from '../../store/reduxHelper';
import { orderActions } from '../../store/actions';

const requestGetOrders = (action, dispatch) => {
  debugger
  OrdersService.getAll()
    .then((res) => {
      const orders = res.data.response;
      dispatch(orderActions.setProductList({ orders }));
    });
};

const EVENTS = {
  [orderActions.GET_ORDERS]: requestGetOrders,
};

const middleware = createMiddleware(EVENTS);

export default middleware;
