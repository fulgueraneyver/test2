import { fromJS } from 'immutable';
import { createReducer } from '../../store/reduxHelper';

import { productActions } from '../../store/actions';

const initialState = fromJS({
  orderList: {},
});

const setOrderList = (state, action) => {
  const { orders } = action.payload;
  const newState = state.set('orderList', orders);
  return newState;
};

const EVENTS = {
  [productActions.ORDER_LIST]: setOrderList,
};

/*
 * @reduxReducer
 */
const reducer = createReducer(initialState, EVENTS);

export default reducer;
