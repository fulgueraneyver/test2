import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fromJS } from 'immutable';
import {
  Checkbox,
} from 'semantic-ui-react';

import { orderActions } from '../../../../store/actions';
import CustomTable from '../../../../components/CustomTable';

const OrdersList = ({ orderList, orderActs }) => {

  useEffect(() => {
    debugger
    orderActs.requestGetOrders();
  }, [orderActs]);

  const headerData = [
    {
      name: <Checkbox
        defaultIndeterminate={false}
      />,
      key: 'Code',
      width: 1,
      component: () => (<div></div>),
    },
    {
      name: 'Orden', key: 'order', width: 2,
    },
    {
      name: 'Fecha', key: 'date', width: 2,
    },
    {
      name: 'Estado', key: 'status', width: 2,
    },
    {
      name: 'Total', key: 'total', width: 2,
    },
  ];
  const example = [
    {
      order: 'neyver',
      date: '10/10/2012',
      status: 'on hold',
      total: '55$',
      checked: false,
      id: 1
    }
  ]
  return (
    <div className="product-table">
      <CustomTable
        columns={headerData}
        data={fromJS(example)}
        checkedRows
        actionsWithHover
        heightTable="60vh"
      />
    </div>
  )
}

OrdersList.propTypes = {
  orderList: PropTypes.shape({}),
};

const mapStateToProps = (state) => {
  const { order } = state;
  const orderList = order.getIn(['orderList', 'orders']) || [];
  return {
    orderList,
  };
};

const mapDispatchToProps = dispatch => ({
  orderActs: bindActionCreators(orderActions, dispatch),
});

const withCompose = compose(
  connect(mapStateToProps, mapDispatchToProps)
);
export default withCompose(OrdersList);