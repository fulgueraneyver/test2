import React from 'react';

import ContainerView from '../../components/ContainerView';
import { PANEL_COLORS } from '../../helpers/constants';
import TableList from './../Products/components/TableView';
import MainLayout from '../../layouts/MainLayout';

const Products = () => {
  return (
    <ContainerView
      leftPanel={{
        color: PANEL_COLORS.LEFT,
      }}
      centerPanel={{
        component:
          <MainLayout>
            <TableList />
          </MainLayout>,
        color: PANEL_COLORS.CENTER,
      }}
    />
  )
}
export default Products;