import { ProductsService } from '../../services';
import { createMiddleware } from '../../store/reduxHelper';
import { productActions } from '../../store/actions';

const requestGetProducts = (action, dispatch) => {
  ProductsService.getAll()
    .then((res) => {
      const products = res.data.response;
      dispatch(productActions.setProductList({ products }));
    });
};

const EVENTS = {
  [productActions.GET_PRODUCTS]: requestGetProducts,
};

const middleware = createMiddleware(EVENTS);

export default middleware;
