import { fromJS } from 'immutable';
import { createReducer } from '../../store/reduxHelper';

import { productActions } from '../../store/actions';

const initialState = fromJS({
  productList: {},
});

const setProductList = (state, action) => {
  const { products } = action.payload;
  const newState = state.set('productList', products);
  return newState;
};

const EVENTS = {
  [productActions.PRODUCT_LIST]: setProductList,
};

/*
 * @reduxReducer
 */
const reducer = createReducer(initialState, EVENTS);

export default reducer;
