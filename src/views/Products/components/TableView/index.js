import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fromJS } from 'immutable';
import {
  Header, Icon, Dropdown, Grid, Segment,
} from 'semantic-ui-react';

import { productActions } from '../../../../store/actions';
import CustomTable from '../../../../components/CustomTable';
import TransformHtml from '../../../../components/TransformHtml';
import { LABELS_PRODUCTS } from '../../../../helpers/constants';
import './style.scss'

const ProductsList = ({ productList, productActs }) => {

  useEffect(() => {
    productActs.requestGetProducts();
  }, [productActs]);

  const headerData = [
    {
      name: 'Codigo', key: 'Code', width: 1,
    },
    {
      name: 'Descripcion',
      key: 'Description',
      width: 5,
      component: state => {
        return TransformHtml(state);
      },
    },
    {
      name: 'Precio', key: 'Price', width: 2,
    },
    {
      name: 'SILVER', key: 'SILVER', width: 2,
    },
    {
      name: 'BRONZE', key: 'BRONZE', width: 2,
    },
    {
      name: 'GOLD', key: 'GOLD', width: 2,
    },
    {
      name: 'PLATINIUM', key: 'PLATINIUM', width: 3,
    },
  ];
  const getOptions = productList.map((option) => {
    const {
      id, label, price, price_ttc, price_min, price_min_ttc, price_base_type
    } = option;
    return {
      Code: id,
      Description: label,
      Price: price,
      SILVER: price_ttc,
      BRONZE: price_min,
      GOLD: price_min_ttc,
      PLATINIUM: price_base_type,
    }
  });
  return (
    <div className="product-table">
      <Header as="h2" >
        {LABELS_PRODUCTS.TITLE}
      </Header>
      <Header as="h4" attached='top'>
        <Icon name='tag' />
        {LABELS_PRODUCTS.SUB_TITLE}
      </Header>
      <Segment attached className='colection-dropdown'>
        <Grid divided='vertically' textAlign='center'>
          <Grid.Row columns={2}>
            <Grid.Column mobile={16} computer={8}>
              <Dropdown clearable options={[]} selection placeholder={LABELS_PRODUCTS.PLACEHOLDER.PRODUCT} />
            </Grid.Column>
            <Grid.Column mobile={16} computer={8}>
              <Dropdown clearable options={[]} selection placeholder={LABELS_PRODUCTS.PLACEHOLDER.BRAND} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
      <CustomTable
        columns={headerData}
        data={fromJS(getOptions)}
      />
    </div>
  )
}

ProductsList.propTypes = {
  productList: PropTypes.shape({}),
};

const mapStateToProps = (state) => {
  const { product } = state;
  const productList = product.getIn(['productList', 'products']) || [];
  return {
    productList,
  };
};

const mapDispatchToProps = dispatch => ({
  productActs: bindActionCreators(productActions, dispatch),
});

const withCompose = compose(
  connect(mapStateToProps, mapDispatchToProps)
);
export default withCompose(ProductsList);