import './App.css';
import Products from '../src/views/Products';
import Orders from '../src/views/Orders';
import { Switch, Route } from 'react-router-dom';
import Login from './layouts/Login';

function App() {
  return (
    <Switch>
      <Route exact path="/productos" component={Products} />
      <Route exact path="/" component={Login} />
      <Route exact path="/orders" component={Orders} />
    </Switch>
  );
}

export default App;
