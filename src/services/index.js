import ServiceManager from './common/ServiceManager';
import ProductsService from './rest/products';
import OrdersService from './rest/orders';

export {
  ServiceManager,
  ProductsService,
  OrdersService,
};
