
import RestService from '../common/RestService';

class ProductService extends RestService {
  static Endpoints = {
    GET_ALL: '/products',
    GET_BRANDS: '/categoria/get-marcas',
    GET_PRODUCTS: '/prices/types-categories'
  };

  constructor() {
    super(ProductService.Endpoints);
  }
  getTypeProduct = () => {
    return this.serviceInstance.get(ProductService.Endpoints.GET_PRODUCTS);
  }
  getBrands = () => {
    return this.serviceInstance.get(ProductService.Endpoints.GET_BRANDS);
  }
}

export default new ProductService();