export const config = {
  headers: {
    'Accept-Version': 1,
    Accept: 'application/vnd.api+json',
    'Content-Type': 'application/json; charset=utf-8',
    DOLAPIKEY: '8db83d45cba108ebeb0db6eae483856e0000eb8d'
  },
  baseURL: 'http://www.tiendasclick.com:3030/api'
};

export default config;
