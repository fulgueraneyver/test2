import React from 'react';
import PropTypes from 'prop-types';

import Header from '../../components/HeaderMenu';

const MainLayout = ({ children }) => {

  return (
    <React.Fragment>
      <Header />
      {children}
    </React.Fragment>
  );
};

MainLayout.propTypes = {
  children: PropTypes.node,
};

MainLayout.defaultProps = {
  children: null,
};

export default MainLayout;
