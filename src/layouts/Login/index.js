import React from 'react';
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react';
import Logo from '../../assets/images/logo-blue.png';

const LoginForm = () => (
  <Grid textAlign='center' style={{ height: '100vh', background: '#e9ecef' }} verticalAlign='middle'>
    <Grid.Column style={{ maxWidth: 450 }}>
      <Header textAlign='center'>
        <Image src={Logo} style={{ width: 250 }} />
      </Header>
      <Form size='large'>
        <Segment stacked>
          <Form.Input fluid icon='user' iconPosition='left' placeholder='Usuario' />
          <Form.Input
            fluid
            icon='lock'
            iconPosition='left'
            placeholder='Contraseña'
            type='password'
          />

          <Button color='blue' fluid size='large'>
            Iniciar sesión
          </Button>
        </Segment>
      </Form>
      <Message>
        ¿Registrar Nuevo? <a href='/#'>registre</a>
      </Message>
    </Grid.Column>
  </Grid>
)

export default LoginForm