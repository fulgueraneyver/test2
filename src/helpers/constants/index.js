export const PANEL_COLORS = {
  LEFT: '#0078ba',
  CENTER: '#f4f6f9',
};

export const LABELS_PRODUCTS = {
  TITLE: 'Fijacion de precios referenciales',
  SUB_TITLE: 'Productos',
  PLACEHOLDER: {
    PRODUCT: 'Tipo de Producto...',
    BRAND: 'Marcas...'
  }
}