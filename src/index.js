import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';
import "semantic-ui-css/semantic.min.css";

import App from './App';
import reportWebVitals from './reportWebVitals';
import rootStore from './store/rootStore';
import './index.css';

ReactDOM.render(
  <HashRouter >
    <Provider store={rootStore}>
      <App />
    </Provider>
  </HashRouter >,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
